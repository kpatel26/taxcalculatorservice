﻿using AutoMapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using TaxCalculator.Contracts.Entities.taxjar;
using TaxCalculator.Contracts.Interfaces;
using Taxjar;

namespace TaxCalculator.Taxjarr.Services
{
    public class TaxjarCalculatorRest : ITaxJarCalculator
    {
        private readonly ILogger<TaxjarCalculatorRest> logger;
        private readonly HttpClient httpClient;
        private readonly IMapper mapper;
        private readonly IConfiguration configuration;
        public TaxjarCalculatorRest(HttpClient httpClient, IConfiguration configuration, IMapper mapper, ILogger<TaxjarCalculatorRest> logger)
        {
            this.logger = logger;
            this.mapper = mapper;
            this.httpClient=httpClient;
            this.configuration = configuration;
        }

        public async Task<CalculatedTax> Calculate(TaxForOrder taxForOrder)
        {
            try
            {
                var postJson = JsonConvert.SerializeObject(taxForOrder);
                var message = PrepareHttpRequest("taxes", HttpMethod.Post, postJson);
                var response = await httpClient.SendAsync(message);
                var data = response.Content.ReadAsStringAsync();
                var taxResult = JsonConvert.DeserializeObject<CalculatedTax>(data.Result);
                return mapper.Map<CalculatedTax>(taxResult);
            }
            catch(Exception e)
            {
                logger.LogError(e, "Error while calling TaxForOrder method to taxjar API!");
                //Should throw Custom exception with user friendly message to the calling layer 
                throw;
            }
        }
        public async Task<TaxRate> GetTaxRate(string zipCode)
        {
            try
            {
                
                var message = PrepareHttpRequest("rates/"+zipCode, HttpMethod.Get);
                var response = await httpClient.SendAsync(message);
                var data = response.Content.ReadAsStringAsync();
                var taxResult = JsonConvert.DeserializeObject<RateResponseAttributes>(data.Result);
                return mapper.Map<TaxRate>(taxResult);

            }
            catch (Exception e)
            {
                logger.LogError(e, "Error while calling TaxForOrder method to taxjar API!");
                //Should throw Custom exception with user friendly message to the calling layer 
                throw;
            }
        }
        public HttpRequestMessage PrepareHttpRequest(string requestUrl, HttpMethod method, string postJson = null)
        {
            string uri = configuration["TaxService:taxjar:BaseUrl"] + requestUrl;
            var requestMessage = new HttpRequestMessage(method, uri);
            requestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", configuration["TaxService:taxjar:ApiKey"]);
            requestMessage.Headers.Add("Accept", "application/json");
            if (method == HttpMethod.Post)
            {
                //construct content to send
                var content = new System.Net.Http.StringContent(postJson, Encoding.UTF8, "application/json");
                requestMessage.Content = content;
            }
            return requestMessage;
        }
    }
}
