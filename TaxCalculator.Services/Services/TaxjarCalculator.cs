﻿using AutoMapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;
using TaxCalculator.Contracts.Entities.taxjar;
using TaxCalculator.Contracts.Interfaces;
using Taxjar;

namespace TaxCalculator.Taxjarr.Services
{
    public class TaxjarCalculator : ITaxJarCalculator
    {
        private readonly ILogger<TaxjarCalculator> logger;
        private readonly TaxjarApi taxjarApi;
        private readonly IMapper mapper;

        public TaxjarCalculator (TaxjarApi taxjarApi, IMapper mapper, ILogger<TaxjarCalculator> logger)
        {
            this.logger = logger;
            this.mapper = mapper;
            this.taxjarApi = taxjarApi;
        }

        public async Task<CalculatedTax> Calculate(TaxForOrder taxForOrder)
        {
            try
            {
                var tax = await taxjarApi.TaxForOrderAsync(taxForOrder);
                return mapper.Map<CalculatedTax>(tax);
            }
            catch (Exception e)
            {
                logger.LogError(e, "Error while calling TaxForOrder method to taxjar API!");
                //Should throw Custom exception with user friendly message to the calling layer 
                throw;
            }
        }
        public async Task<TaxRate> GetTaxRate(string zipCode)
        {
            try
            {
                var tax = await taxjarApi.RatesForLocationAsync(zipCode);
                return mapper.Map<TaxRate>(tax);
            }
            catch (Exception e)
            {
                logger.LogError(e, "Error while calling TaxForOrder method to taxjar API!");
                //Should throw Custom exception with user friendly message to the calling layer 
                throw;
            }
        }



    }
}
