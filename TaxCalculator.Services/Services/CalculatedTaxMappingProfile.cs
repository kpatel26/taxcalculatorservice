﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using TaxCalculator.Contracts.Entities.taxjar;
using TaxCalculator.Contracts.Models;
using Taxjar;

namespace TaxCalculator.Taxjarr.Services
{
    public class CalculatedTaxMappingProfile : Profile
    {
        public CalculatedTaxMappingProfile()
        {
            CreateMap<CalculatedTax, TaxResponseAttributes>(MemberList.None);
            CreateMap<TaxResponseAttributes, CalculatedTax>(MemberList.None);
        }
    }
}
