﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using TaxCalculator.Contracts.Entities.taxjar;
using TaxCalculator.Contracts.Models;
using Taxjar;

namespace TaxCalculator.Taxjarr.Services
{
    public class TaxRateMappingProfile : Profile
    {
        public TaxRateMappingProfile()
        {
            CreateMap<TaxRate, RateResponseAttributes>(MemberList.None);
            CreateMap<RateResponseAttributes, TaxRate>(MemberList.None);
        }
    }
}
