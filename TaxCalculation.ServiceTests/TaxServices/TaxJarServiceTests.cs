﻿using Xunit;
using TaxCalculation.Service.TaxServices;
using System;
using System.Collections.Generic;
using System.Text;
using Moq;
using AutoMapper;
using AutoMapper.Configuration;
using Microsoft.Extensions.Logging;
using TaxCalculator.Taxjarr.Services;
using Taxjar;
using TaxCalculator.Contracts.Interfaces;
using TaxCalculator.Contracts.Models;
using TaxCalculator.Contracts.Entities.taxjar;

namespace TaxCalculation.Service.TaxServices.Tests
{
    public class TaxJarServiceTests
    {
        protected MockRepository mockRepository;
        protected Mock<IMapper> mockMapper;
        protected Mock<IConfiguration> mockConfiguration;
        protected Mock<TaxjarApi> mockTaxjatApi;
        protected Mock<ILogger<TaxjarCalculator>> mockLogger;
        protected Mock<ILogger<TaxJarService>> mockLoggerService;
        protected Mock<ITaxJarCalculator> mockTaxjarCalculator;

        public TaxJarServiceTests()
        {
            this.mockRepository = new MockRepository(MockBehavior.Default);
            this.mockMapper = this.mockRepository.Create<IMapper>();
            this.mockConfiguration = this.mockRepository.Create<IConfiguration>();
            this.mockTaxjarCalculator = this.mockRepository.Create<ITaxJarCalculator>();
            this.mockTaxjatApi = new Mock<TaxjarApi>(MockBehavior.Default, "asasasasasasasasasasasasasasas", null);
            this.mockLogger = this.mockRepository.Create<ILogger<TaxjarCalculator>>();
            this.mockLoggerService = this.mockRepository.Create<ILogger<TaxJarService>>();
        }

        [Fact()]
        public void TaxJarService_Test_Constructor()
        {
            //basic test for constructor
            var constructObject = new TaxJarService(mockTaxjarCalculator.Object, mockMapper.Object, mockLoggerService.Object);
            Assert.NotNull(constructObject);

        }


        [Fact()]
        public void CalculateTax_Test_to_verify_Code_Pass_through()
        {
            var taxForOrderVM = new TaxForOrderViewModel()
            {
                amount = 100
            };
            var taxForOrder = new TaxForOrder()
            {
                amount = 100
            };
            var taxReturn = new CalculatedTax() { AmountToCollect = 120, TaxableAmount = 100, Shipping = 10, Rate = 10 };
            mockTaxjarCalculator.Setup(x => x.Calculate(taxForOrder)).ReturnsAsync(taxReturn).Verifiable();

            var taxjarService = new TaxJarService(this.mockTaxjarCalculator.Object, this.mockMapper.Object, mockLoggerService.Object);
            var calculatedTax = taxjarService.CalculateTax(taxForOrderVM);

            Assert.True(true, "Test Failed!");
        }

        

    }
}