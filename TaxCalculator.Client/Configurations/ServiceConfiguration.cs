﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;
using AutoMapper;
using Taxjar;
using TaxCalculator.Contracts.Interfaces;
using TaxCalculator.Taxjarr.Services;
using TaxCalculation.Service.TaxServices;
using Serilog;
using System.IO;
using System;
using System.Net.Http;

namespace TaxCalculator.Client.Configurations
{
    public static class ServiceConfiguration
    {
        public static IServiceCollection ConfigureServices(this IServiceCollection services, IConfiguration configuration)
        {
            var taxCalculationAssembly = Assembly.Load("TaxCalculation.Service");
            var taxjarAssembly = Assembly.Load("TaxCalculator.Taxjarr");
            var contractsAssembly = Assembly.Load("TaxCalculator.Contracts");
            

            services.AddLogging(configure => configure.AddSerilog());

            services.AddAutoMapper(taxCalculationAssembly, taxjarAssembly, contractsAssembly);

            //services.AddTransient<TaxjarApi>(x => new TaxjarApi(configuration["TaxService:taxjar:ApiKey"]));
            //services.AddTransient<ITaxJarCalculator, TaxjarCalculator>(); 
            
            services.AddTransient<HttpClient>();
            services.AddTransient<ITaxJarCalculator, TaxjarCalculatorRest>();

            services.AddTransient<ITaxCalculationService, TaxJarService>();
            //Add other Tax calculation services
            //services.AddScoped<ITaxCalculationService, AvalaraService>();

            return services;
        }
    }

}
