﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using TaxCalculator.Client.Application;
using TaxCalculator.Client.Configurations;
using TaxCalculator.Contracts.Interfaces;

namespace TaxCalculator.Client
{
    class Program
    {
        private static IEnumerable<ITaxCalculationService> taxCalculationServices;

        static void Main(string[] args)
        {
            SetupConfiguration();
            TaxCalculationConsole.StartConsole(taxCalculationServices);

        }
        private static void SetupConfiguration()
        { 
            //Build Configuration
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
            var configuration = builder.Build();

            // Create a service collection and configure our depdencies
            var serviceCollection = new ServiceCollection();
            serviceCollection.AddSingleton<IConfiguration>(configuration);
            serviceCollection.ConfigureServices(configuration);
            var serviceProvider = serviceCollection.BuildServiceProvider(true);
            
            taxCalculationServices = serviceProvider.GetServices<ITaxCalculationService>();

            //Just to log from program
            var logger = serviceProvider.GetService<ILoggerFactory>()
                .CreateLogger<Program>();
            logger.LogDebug("Starting application");
            logger.LogDebug("All done!");
        }
    }
}
