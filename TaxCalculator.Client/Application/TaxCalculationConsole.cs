﻿using System;
using System.Collections.Generic;
using System.Text;
using TaxCalculator.Client.ServiceContext;
using TaxCalculator.Contracts.Enums;
using TaxCalculator.Contracts.Interfaces;
using TaxCalculator.Contracts.Models;

namespace TaxCalculator.Client.Application
{
    public class TaxCalculationConsole
    {
        public static void StartConsole(IEnumerable<ITaxCalculationService> taxCalculationServices)
        {
            do
            {
                var taxOrder = CaptureOrderDetail();
                Console.WriteLine("\nPress any key to calculate Tax...");
                Console.ReadLine();

                var taxCalculator = new TaxServiceContext(taxCalculationServices);
                var calculatedTax= taxCalculator.CalculateTax(taxOrder, taxOrder.ServiceType).Result;

                Console.WriteLine("\nCalculated Tax amount : " + calculatedTax.amount_to_collect.ToString() );

                Console.WriteLine("\nPress ESC to exit or any key to continue...");
                Console.ReadLine();

            } while (Console.ReadKey().Key != ConsoleKey.Escape);

        }

        private static TaxForOrderViewModel CaptureOrderDetail()
        {
            var taxOrder = new TaxForOrderViewModel();

            Console.WriteLine("\n*********Welcome to Tax Calculation Console*********");
            Console.WriteLine("\nFrom_country	: ");
            taxOrder.from_country = Console.ReadLine();
            Console.WriteLine("\nFrom_zip	: ");
            taxOrder.from_zip = Console.ReadLine();
            Console.WriteLine("\nFrom_state	: ");
            taxOrder.from_state = Console.ReadLine();
            Console.WriteLine("\nFrom_city	: ");
            taxOrder.from_city = Console.ReadLine();
            Console.WriteLine("\nTo_country	: ");
            taxOrder.to_country = Console.ReadLine();
            Console.WriteLine("\nTo_zip	: ");
            taxOrder.to_zip = Console.ReadLine();
            Console.WriteLine("\nTo_state	: ");
            taxOrder.to_state = Console.ReadLine();
            Console.WriteLine("\nTo_city	: ");
            taxOrder.to_city = Console.ReadLine();
            Console.WriteLine("\nAmount	: ");
            taxOrder.amount = decimal.Parse(Console.ReadLine());
            Console.WriteLine("\nTax Calculation Service [1-Taxjar, 2-Avalara] : ");
            taxOrder.ServiceType = (TaxServiceType)Enum.Parse(typeof(TaxServiceType), Console.ReadLine());

            return taxOrder;
        }
    }
}
