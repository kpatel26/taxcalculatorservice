﻿using Xunit;
using Moq;
using AutoMapper;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;
using TaxCalculator.Contracts.Entities.taxjar;
using TaxCalculator.Taxjarr.Services;
using System.Net.Http;
using System.Text;
using System.Net.Http.Headers;
using System;
using Newtonsoft.Json;

namespace Taxjar.Calculator.Services.Tests
{
    public class TaxjarCalculatorTests
    {

        protected MockRepository mockRepository;
        protected Mock<IMapper> mockMapper;
        protected Mock<IConfiguration> mockConfiguration;
        protected Mock<TaxjarApi> mockTaxjarApi;
        protected Mock<ILogger<TaxjarCalculator>> mockLogger;


        public TaxjarCalculatorTests()
        {
            this.mockRepository = new MockRepository(MockBehavior.Default);
            this.mockMapper = this.mockRepository.Create<IMapper>();
            this.mockConfiguration = this.mockRepository.Create<IConfiguration>();

            this.mockTaxjarApi = new Mock<TaxjarApi>(MockBehavior.Default, "asasasasasasasasasasasasasasas", null);
            this.mockLogger = this.mockRepository.Create<ILogger<TaxjarCalculator>>();

        }


        [Fact()]
        public void TaxjarCalculator_Constructor_Basic_Test()
        {
            //just basic test for constructor
            var constructObject = new TaxjarCalculator(mockTaxjarApi.Object, mockMapper.Object, mockLogger.Object);
            Assert.NotNull(constructObject);

        }

        [Fact()]
        public void CalculateTest_Verify_That_The_Code_passes_without_exceptions()
        {
            var taxForOrder = new TaxForOrder()
            {
                amount = 100
            };

            var taxReturn = new TaxResponseAttributes() { AmountToCollect = 120, TaxableAmount = 100, Shipping = 10, Rate = 10 };
            mockTaxjarApi.Setup(x => x.TaxForOrderAsync(taxForOrder)).ReturnsAsync(taxReturn).Verifiable();

            var taxjatCalculator = new TaxjarCalculator(this.mockTaxjarApi.Object, this.mockMapper.Object, mockLogger.Object);
            var calculatedTax = taxjatCalculator.Calculate(taxForOrder);

            //check to see how many times the method was called
            //mockTaxjarApi.Verify( mr => mr.TaxForOrder(taxForOrder), Times.Once());
            Assert.True(true, "Test Failed!");
        }

        [Fact()]
        public void CalculateTest_And_Verify_The_Result_Based_On_Supplied_Argument()
        {
            var taxForOrder = new TaxForOrder()
            {
                amount = 100
            };

            var taxReturn = new TaxResponseAttributes() { AmountToCollect = 120, TaxableAmount = 100, Shipping = 10, Rate = 10 };
            mockTaxjarApi.Setup(x => x.TaxForOrderAsync(taxForOrder)).ReturnsAsync(taxReturn).Verifiable();

            //Supply actual mapper to TaxjarCalculator so that mapper creates retruned object from mock result 
            var config = new MapperConfiguration(cfg => cfg.CreateMap<TaxResponseAttributes, CalculatedTax>());
            var mapper = new Mapper(config);

            var taxjatCalculator = new TaxjarCalculator(this.mockTaxjarApi.Object, mapper, mockLogger.Object);
            var calculatedTax = taxjatCalculator.Calculate(taxForOrder).Result;

            var isExpectedResult = calculatedTax.TaxableAmount == 100 && calculatedTax.AmountToCollect == 120;

            Assert.True(isExpectedResult, "Test Failed!");
        }


        [Fact()]
        public void CalculateTax_Integration_Test()
        {
            var taxForOrder = new TaxForOrder()
            {
                amount = 100,
                from_zip = "30041",
                to_zip = "60018",
                from_city = "Atlanta",
                from_state = "GA",
                from_country = "US",
                to_city = "CHICAGO",
                to_state = "IL",
                to_country = "US"
            };

            var taxjarApi = new TaxjarApi("5da2f821eee4035db4771edab942a4cc");
            //Supply actual mapper to TaxjarCalculator so that mapper creates retruned object from mock result 
            var config = new MapperConfiguration(cfg => cfg.CreateMap<TaxResponseAttributes, CalculatedTax>());
            var mapper = new Mapper(config);

            var taxjatCalculator = new TaxjarCalculator(taxjarApi, mapper, mockLogger.Object);
            var calculatedTax = taxjatCalculator.Calculate(taxForOrder).Result;

            var isExpectedResult = calculatedTax.TaxableAmount == 0 && calculatedTax.AmountToCollect == 0;

            Assert.True(isExpectedResult, "Test Failed!");
        }

        [Fact]
        public void test_http_client()
        {
            try
            {
                var taxForOrder = new TaxForOrder()
                {
                    amount = 100,
                    from_zip = "30041",
                    to_zip = "60018",
                    from_city = "Atlanta",
                    from_state = "GA",
                    from_country = "US",
                    to_city = "CHICAGO",
                    to_state = "IL",
                    to_country = "US"
                };
                HttpClient httpClient = new HttpClient();
                var postJson = JsonConvert.SerializeObject(taxForOrder);
                var message = PrepareHttpRequest("taxes", HttpMethod.Post, postJson);
                var response = httpClient.SendAsync(message).Result;

                var data = response.Content.ReadAsStringAsync().Result;

                //return JsonConvert.DeserializeObject<List<TResult>>(data);
            }
            catch(Exception e)
            { }
        }

        public HttpRequestMessage PrepareHttpRequest(string requestUrl, HttpMethod method, string postJson=null)
        {
            string uri = "https://api.taxjar.com/v2/" + requestUrl;
            var requestMessage = new HttpRequestMessage(method, uri);
            requestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", "e3927003f7ee46f8c688472f80bd8fa2");
            requestMessage.Headers.Add("Accept", "application/json");
            if (method == HttpMethod.Post)
            {
                //construct content to send
                var content = new System.Net.Http.StringContent(postJson, Encoding.UTF8, "application/json");
                requestMessage.Content = content;
            }
            return requestMessage;
        }

    }
}