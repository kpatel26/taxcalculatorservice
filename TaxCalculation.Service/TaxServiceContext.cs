﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using TaxCalculator.Contracts.Interfaces;
using TaxCalculator.Contracts.Enums;
using TaxCalculation.Service.TaxServices;
using TaxCalculator.Contracts.Entities.taxjar;
using TaxCalculator.Contracts.Models;
using System.Threading.Tasks;

namespace TaxCalculator.Client.ServiceContext
{
    /// <summary>
    /// TaxServiceContext Resolves the Tax Service instance based on TaxServiceType paranmater, Make a call to service for requested operation 
    /// </summary>
    public class TaxServiceContext 
    {
        private readonly IEnumerable<ITaxCalculationService> taxCalculationServices;
        private ITaxCalculationService taxCalculationService;

        /// <summary>
        /// TaxServiceContext : Constructor injects all concreate implementation of IEnumerable<ITaxCalculationService>
        /// and resolve requires service based on TaxServiceType
        /// </summary>
        /// <param name="taxCalculationServices"></param>
        public TaxServiceContext(IEnumerable<ITaxCalculationService> taxCalculationServices)
        {
            this.taxCalculationServices = taxCalculationServices;
        }

        /// <summary>
        /// CalculateTax calcuates tax from the requested Tax calculation provider and returns result
        /// </summary>
        /// <param name="taxForOrder"></param>
        /// <param name="taxServiceType"></param>
        /// <returns></returns>
        public async Task< CalculatedTaxViewModel> CalculateTax(TaxForOrderViewModel taxForOrder ,TaxServiceType taxServiceType)
        {
            
                GetTaxService(taxServiceType);
                return await taxCalculationService.CalculateTax(taxForOrder);
            
        }

        /// <summary>
        /// GetTaxRate retrives TaxRates based on location zipcode from requested provider
        /// </summary>
        /// <param name="zipCode"></param>
        /// <param name="taxServiceType"></param>
        /// <returns></returns>
        public async Task<TaxRateViewModel> GetTaxRate(string zipCode, TaxServiceType taxServiceType)
        {

            GetTaxService(taxServiceType);
            return await taxCalculationService.GetTaxRate(zipCode);

        }

        /// <summary>
        /// GetTaxService resolves service instance
        /// </summary>
        /// <param name="taxServiceType"></param>
        private void GetTaxService(TaxServiceType  taxServiceType)
        {
            //TODO : Find and Return the service instance based on Type  
            taxCalculationService = taxCalculationServices.First(o => o.GetType() == typeof(TaxJarService));
            
        }

    }
}
