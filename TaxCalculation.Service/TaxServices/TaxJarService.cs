﻿using AutoMapper;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using TaxCalculator.Contracts.Entities.taxjar;
using TaxCalculator.Contracts.Interfaces;
using TaxCalculator.Contracts.Models;

namespace TaxCalculation.Service.TaxServices
{
    public class TaxJarService : ITaxCalculationService
    {
        private readonly ITaxJarCalculator taxJarCalculator;
        private readonly IMapper mapper;
        private readonly ILogger<TaxJarService> logger;

        public TaxJarService(ITaxJarCalculator taxJarCalculator, IMapper mapper , ILogger<TaxJarService> logger)
        {
            this.taxJarCalculator = taxJarCalculator;
            this.mapper = mapper;
            this.logger = logger;
        }
        public async Task<CalculatedTaxViewModel> CalculateTax(TaxForOrderViewModel taxForOrderViewModel)
        {
            var taxOrder = mapper.Map<TaxForOrder>(taxForOrderViewModel);
            var calculatedTax = await taxJarCalculator.Calculate(taxOrder);
            return mapper.Map<CalculatedTaxViewModel>(calculatedTax);
        }
        public async Task< TaxRateViewModel> GetTaxRate(string zipCode)
        {           
            var taxRate = await taxJarCalculator.GetTaxRate(zipCode);
            return mapper.Map<TaxRateViewModel>(taxRate);
        }
    }
}
