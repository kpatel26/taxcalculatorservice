﻿using System;
using System.Collections.Generic;
using System.Text;
using TaxCalculator.Contracts.Enums;

namespace TaxCalculator.Contracts.Models
{
    public class TaxForOrderViewModel
    {
        public string from_country { get; set; }
        public string from_zip { get; set; }
        public string from_state { get; set; }
        public string from_city { get; set; }
        public string to_country { get; set; }
        public string to_zip { get; set; }
        public string to_state { get; set; }
        public string to_city { get; set; }
        public decimal amount { get; set; }
        public decimal shipping { get; set; }
        public TaxServiceType ServiceType { get; set; }
        //public virtual ICollection<NexusAddress> NexusAddresses { get; set; }
        //public virtual ICollection<LineItem> LineItems { get; set; }
    }
}
