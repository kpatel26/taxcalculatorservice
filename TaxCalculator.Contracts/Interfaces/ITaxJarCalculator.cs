﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TaxCalculator.Contracts.Entities.taxjar;

namespace TaxCalculator.Contracts.Interfaces
{
    public interface ITaxJarCalculator
    {
        Task<CalculatedTax> Calculate(TaxForOrder taxForOrder);
        Task<TaxRate> GetTaxRate(string zipCode);
    }
}
