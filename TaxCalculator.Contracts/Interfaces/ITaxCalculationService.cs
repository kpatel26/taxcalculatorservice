﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TaxCalculator.Contracts.Models;

namespace TaxCalculator.Contracts.Interfaces
{
    public interface ITaxCalculationService
    {
        Task<CalculatedTaxViewModel> CalculateTax(TaxForOrderViewModel taxForOrderViewModel);
        Task<TaxRateViewModel> GetTaxRate(string zipCode);
    }
}
