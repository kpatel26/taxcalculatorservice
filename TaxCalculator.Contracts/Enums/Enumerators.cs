﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaxCalculator.Contracts.Enums
{

    public enum TaxServiceType
    {
        Taxjar = 1,
        Avalara = 2,
        Ziptax = 3
    }


}
