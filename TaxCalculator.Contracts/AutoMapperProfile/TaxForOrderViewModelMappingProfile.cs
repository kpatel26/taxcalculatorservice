﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using TaxCalculator.Contracts.Entities.taxjar;
using TaxCalculator.Contracts.Models;

namespace TaxCalculator.Contracts.AutoMapperProfile
{
    public class TaxForOrderViewModelMappingProfile : Profile
    {
        public TaxForOrderViewModelMappingProfile()
        {
            CreateMap<TaxForOrder, TaxForOrderViewModel>(MemberList.Destination)
                .ForMember(x => x.ServiceType, opt => opt.Ignore()); ;
            CreateMap<TaxForOrderViewModel, TaxForOrder>(MemberList.Destination)
                ;
                
        }
    }
}
