﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using TaxCalculator.Contracts.Entities.taxjar;
using TaxCalculator.Contracts.Models;

namespace TaxCalculator.Taxjarr.Services
{
    public class TaxRateMappingProfile : Profile
    {
        public TaxRateMappingProfile()
        {
            CreateMap<TaxRate, TaxRateViewModel>(MemberList.None);
            CreateMap<TaxRateViewModel, TaxRate>(MemberList.None);
        }
    }
}
