﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using TaxCalculator.Contracts.Entities.taxjar;
using TaxCalculator.Contracts.Models;

namespace TaxCalculator.Taxjarr.Services
{
    public class CalculatedTaxMappingProfile : Profile
    {
        public CalculatedTaxMappingProfile()
        {
            CreateMap<CalculatedTax, CalculatedTaxViewModel>(MemberList.None);
            CreateMap<CalculatedTaxViewModel, CalculatedTax>(MemberList.None);
        }
    }
}
